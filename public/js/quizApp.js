app.controller('MainController', function($scope, $http, $timeout, $location, questionID, altVal){

    $scope.init = function(){

    };

    $scope.checkAnswer = function(val){
        var messages = $('div#messages');
        if(val == altVal)
        {
            messages.removeClass('CPerror');
            messages.html('Dat is goed! <img class="character right" src="/img/character/goed.png">').addClass('CPsuccess');

            var data ={
                _token: $('input[name=_token]').val(),
                questionID: questionID,
                value: $('span#mainScore').text()
            };

            $http.post('/quiz/rightAnswer', data).success(function(response){
            if(response.code == 200){
                console.log('Update + Score');
                $timeout(function(){
                    window.location('/quiz')
                }, 3000)
            }


            });
        }else{
            messages.removeClass('CPsuccess');
            messages.html('<img class="character left" src="/img/character/fout.png"> Oeps! Dat is niet goed! :(').addClass('CPerror');
            var data ={
                _token: $('input[name=_token]').val(),
                questionID: questionID
            };
            $http.post('/quiz/wrongAnswer', data).success(function(response){
                if(response.code == 200){
                    console.log('Update');
                    $timeout(function(){
                        window.location('/quiz')
                    }, 3000)
                }


            });
        }
    };

    $scope.init();

});
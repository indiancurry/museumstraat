$(document).on('ready', init);

function init(){
    $('.badgeImage').balloon({
        css: {
            minWidth: "20px",
            padding: "5px",
            borderRadius: "6px",
            border: "solid 1px #777",
            boxShadow: "4px 4px 4px #555",
            color: "#666",
            backgroundColor: "#fff",
            opacity: "0.85",
            zIndex: "32767",
            textAlign: "left"
        }
    });
}
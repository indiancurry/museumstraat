@extends('layouts.master')

@section('content')
    <div class="row">
        <div class="large-12 medium-12 small-12 columns mainContent">
            <div class="gradient sliderHeading"><h2 class="sliderTitle">Dashboard</h2></div>
            <div class="panel">
                <p class="description">Hieronder kun je een Museumstraat, vragen en meer toevoegen.</p>
                <ul class="no-bullet adminDash">
                    <li><a href="/admin/gebruikers"><i class="fi-torso"> Gebruikers</i></a></li>
                    <li><a href="/admin/museumstraat"><i class="fi-home"> Museumstraat</i></a></li>
                    <li><a href="/admin/vragen"><i class="fi-lightbulb"> Vragen</i></a></li>
                </ul>
            </div>
        </div>

    </div>
@stop
@extends('layouts.master')

@section('content')
    <div class="row">
        <div class="small-12 columns mainContent">
            <div class="gradient sliderHeading"><h2 class="sliderTitle">Gebruikers</h2></div>
            <div class="panel">
                <p class="description">Hier zie je alle Museumstraat Quiz-gebruikers. Je kunt gebruikers ook admin maken.</p>

                <table id="leaderboard">
                    <thead>
                    <tr>
                        <th>Naam</th>
                        <th>Rol</th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($users as $u)
                        <tr>
                            <td>{{ $u->name }}</td>
                            @if($u->role == 0)
                                <td>Gebruiker<br><a href="/admin/makeAdmin/{{ $u->id }}">Maak Admin</a></td>

                            @else
                            <td>Admin</td>
                            @endif
                        </tr>
                    @endforeach


                    </tbody>
                </table>

            </div>
        </div>

    </div>
@stop
@extends('layouts.master')

@section('content')
    <div class="row">
        <div class="large-12 medium-12 small-12 columns mainContent">
            <div class="gradient sliderHeading"><h2 class="sliderTitle">Dashboard</h2></div>
            <div class="panel">
                <p class="description">Huidige Museumstraten:</p>
                <ul class="no-bullet">
                    @foreach($museumstraat as $m)
                        <li>{{ $m->name }}</li>
                    @endforeach
                </ul>
                <p class="description">Voeg hier een Museumstraat toe.</p>

                {!! Form::open(array('url' => '/admin/storeMuseumstraat')) !!}
                {!! Form::token() !!}
                {!! Form::text('name') !!}
                {!! Form::submit('Opslaan', $attributes = array('class' => 'button')) !!}
                {!! Form::close() !!}
            </div>
        </div>

    </div>
@stop
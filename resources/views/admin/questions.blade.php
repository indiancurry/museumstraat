@extends('layouts.master')

@section('content')
    <div class="row">
        <div class="large-12 medium-12 small-12 columns mainContent">
            <div class="gradient sliderHeading"><h2 class="sliderTitle">Vragen</h2></div>
            <div class="panel">
                <p class="description">Huidige Vragen:</p>
                <ul class="no-bullet">
                    @foreach($vragen as $v)
                        <li>{{ $v->question }}</li>
                    @endforeach
                </ul>
                <p class="description">Voeg hier een vraag toe.</p>

                {!! Form::open(array('url' => '/admin/postQuestion')) !!}
                {!! Form::token() !!}
                {!! Form::label('question', 'Vraag') !!}
                {!! Form::text('question') !!}
                {!! Form::label('A', 'Antwoord A') !!}
                {!! Form::text('A') !!}
                {!! Form::radio('correctAnswer', 'A', false) !!} <i class="fi-check correctAnswer"> </i><br>
                {!! Form::label('B', 'Antwoord B') !!}
                {!! Form::text('B') !!}
                {!! Form::radio('correctAnswer', 'B', false) !!} <i class="fi-check correctAnswer"> </i><br>
                {!! Form::label('C', 'Antwoord C') !!}
                {!! Form::text('C') !!}
                {!! Form::radio('correctAnswer', 'C', false) !!} <i class="fi-check correctAnswer"> </i><br>
                {!! Form::label('D', 'Antwoord D') !!}
                {!! Form::text('D') !!}
                {!! Form::radio('correctAnswer', 'D', false) !!} <i class="fi-check correctAnswer"> </i><br>
                {!! Form::number('value', '250') !!}
                {!! Form::submit('Opslaan', $attributes = array('class' => 'button')) !!}
                {!! Form::close() !!}
            </div>
        </div>

    </div>
@stop
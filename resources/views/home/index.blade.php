@extends('layouts.master')

@section('content')
    <div class="row">
        <div class="small-12 columns mainContent">
            <div class="gradient sliderHeading"><h2 class="sliderTitle">Musea dit jaar</h2></div>
            <div class="mainSlider">
                <div><img src="{{ URL::asset('img/musea/booijmans.png') }}"></div>
                <div><img src="{{ URL::asset('img/musea/kunsthal.png') }}"></div>
                <div><img src="{{ URL::asset('img/musea/chabot.png') }}"></div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="small-12 columns">
            <iframe class="gMaps" src="https://www.google.com/maps/d/u/0/embed?mid=zSKI2BkRqF_0.k8RgpwBzaEHk"></iframe>
            <button class="button radius fullWidthButton">Verder met quiz</button>
            <div class="row collapse">
                <div class="small-6 columns">
                    <button class="button radius left fbButton socialLogin"><i class="fi-social-facebook"></i> Login met Facebook</button>
                </div>
                <div class="small-6 columns">
                    <button class="button radius right twButton socialLogin"><i class="fi-social-twitter"></i> Login met Twitter</button>
                </div>
            </div>
        </div>
    </div>
@stop
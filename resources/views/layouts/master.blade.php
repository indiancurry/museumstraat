<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>De Museumstraat Quiz</title>
    <link rel="manifest" href="{{ URL::asset('manifest.json') }}">
    <link rel="stylesheet" href="{{ URL::asset('css/foundation.min.css') }}" />
    <link rel="stylesheet" href="{{ URL::asset('css/foundation-icons/foundation-icons.css') }}" />
    <link rel="stylesheet" href="{{ URL::asset('css/main.css') }}" />
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.5.0/slick.css"/>
    <script src="{{ URL::asset('js/vendor/modernizr.js') }}"></script>
</head>
<body>
    {{--<nav class="top-bar sticky topNav" data-topbar role="navigation">--}}
        {{--<ul class="title-area">--}}
            {{--<li class="name">--}}
                {{--<h1><a href="#">Museumstraat Quiz</a></h1>--}}
            {{--</li>--}}
            {{--<!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->--}}
            {{--<li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>--}}
        {{--</ul>--}}
    {{--</nav>--}}

    <div class="off-canvas-wrap" data-offcanvas>
        <div class="inner-wrap">
            <nav class="tab-bar sticky gradient">
                <section class="left-small">
                    <a class="left-off-canvas-toggle menu-icon" href="#"><span></span></a>
                </section>

                <section class="middle tab-bar-section">
                    <h1 class="title">Museumstraat Quiz</h1>
                </section>

                <section class="rightTop">

                    <strong>
                        @if(\Auth::user())
                            <i class="fi-crown scoreIconRaw"> {{ \Auth::user()->score }}</i>
                        @endif
                    </strong>
                </section>
            </nav>

            <aside class="left-off-canvas-menu">
                <ul class="off-canvas-list">
                    <li><a href="/"><label>Home</label></a></li>
                    <li><a href="/plattegrond">Plattegrond</a></li>
                    <li><a href="/quiz">Quiz</a></li>
                    <li><a href="/ranglijst">Ranglijst</a></li>
                </ul>
            </aside>

            <section class="main-section">
                @yield('content')
            </section>

            <a class="exit-off-canvas"></a>

        </div>
    </div>
    @yield('footer')


<script src="{{ URL::asset('js/vendor/jquery.js') }}"></script>
<script src="{{ URL::asset('js/foundation.min.js') }}"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.5.0/slick.min.js"></script>

<script>
    $(document).foundation();
</script>
<script type="text/javascript" src="{{ URL::asset('js/main.js') }}"></script>
@yield('scripts')
</body>
</html>

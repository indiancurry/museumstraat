@extends('layouts.master')

@section('content')
    <div class="row questionWindow" ng-app="Quiz" ng-controller="MainController">
        <div class="small-12 columns mainContent">
            <div class="gradient sliderHeading"><h2 class="sliderTitle">{{ $question->id }}/{{ $total }}</h2><i class="fi-crown scoreIcon"> <span id="mainScore">{{ $question->value }}</span></i></div>
            {!! Form::token() !!}
            <div class="panel">
                <p class="description">{{ $question->question }}</p>
                <div class="progress small-12 pBar">
                    <span class="meter" style="width: 65%"><span class="ball right"></span></span>
                </div>
                <div id="messages"></div>
            </div>
        </div>
        <div class="small-12 columns">
            <button class="button radius fullWidthButton multipleChoiceButton letterA" ng-click="checkAnswer('A')">{{ $question->A }}</button>
            <button class="button radius fullWidthButton multipleChoiceButton letterB" ng-click="checkAnswer('B')">{{ $question->B }}</button>
            <button class="button radius fullWidthButton multipleChoiceButton letterC" ng-click="checkAnswer('C')">{{ $question->C }}</button>
            <button class="button radius fullWidthButton multipleChoiceButton letterD" ng-click="checkAnswer('D')">{{ $question->D }}</button>

        </div>
    </div>
@stop

@section('footer')
    <footer class="badgesPanel">
        <div class="row collapse">
            <div class="small-4 columns badgeIcon badgeDivider">
                <img class="badgeImage" src="{{ URL::asset('img/badges/parkeren.png') }}"  title="Parkeer de vraag voor later!"> <i class="fi-crown scoreIconSmall"> 15</i>
            </div>
            <div class="small-4 columns badgeIcon badgeDivider">
                <img class="badgeImage" src="{{ URL::asset('img/badges/verdubbelaar.png') }}"  title="Zeker van je zaak? Verdubbel de punten!"> <i class="fi-crown scoreIconSmall"> 15</i>
            </div>
            <div class="small-4 columns badgeIcon">
                <img class="badgeImage" src="{{ URL::asset('img/badges/joker.png') }}"  title="Steel de punten van deze vraag en ga direct door naar de volgende!"> <i class="fi-crown scoreIconSmall"> 15</i>
            </div>
        </div>
    </footer>
@stop

@section('scripts')


    <script src="{{ URL::asset('js/vendor/angular.min.js') }}"></script>
    <script src="{{ URL::asset('js/vendor/balloon.js') }}"></script>
    <script src="{{ URL::asset('js/question.js') }}"></script>
    <script>
        var app = angular.module('Quiz', []);
        app.constant("questionID", {{ $question->id }} );
        app.constant("altVal", "{{ $question->correctAnswer }}");
    </script>
    <script src="{{ URL::asset('js/quizApp.js') }}"></script>
@stop
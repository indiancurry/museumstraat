@extends('layouts.master')

@section('content')
    <div class="row questionWindow">
        <div class="small-12 columns mainContent">
            <div class="gradient sliderHeading"><h2 class="sliderTitle">Klaar!</h2></div>
            <div class="panel">
                <p class="description">Je hebt alle vragen beantwoord! Goed gedaan!</p>
                <br>
                <h3>Score</h3>
                <i class="fi-crown scoreIconRaw"></i> <strong class="finalScore">{{ \Auth::user()->score }}</strong>
                <hr>
                Kijk <a href="/ranglijst">hier</a> hoe je in de ranglijst staat!
            </div>
        </div>
    </div>
@stop
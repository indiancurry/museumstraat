@extends('layouts.master')

@section('content')
    <div class="row">
        <div class="small-12 columns mainContent">
            <div class="gradient sliderHeading"><h2 class="sliderTitle">Ranglijst</h2></div>
            <div class="panel">
                <p class="description">Hier zie je de beste Museumstraat Quiz-spelers. Sta jij in de top 100?</p>

                <table id="leaderboard">
                    <thead>
                    <tr>
                        <th>Rang</th>
                        <th>Naam</th>
                        <th>Score</th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($leaderboard as $k=>$v)
                        <tr>
                            <td class="rank">{{ $k+1 }}</td>
                            <td>{{ $v->name }}</td>
                            <td><i class="fi-crown scoreIconSmall"></i>{{ $v->score }}</td>
                        </tr>
                    @endforeach


                    </tbody>
                </table>

            </div>
        </div>

    </div>
@stop
<?php namespace Museumstraat;

use Illuminate\Database\Eloquent\Model;

class UserHasPowerup extends Model {

    protected $fillable = ['user_id', 'powerup'];
    protected $table = 'user_has_powerup';
    public $timestamps = false;

}

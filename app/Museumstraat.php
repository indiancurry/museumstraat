<?php namespace Museumstraat;

use Illuminate\Database\Eloquent\Model;

class Museumstraat extends Model {

    protected $fillable = ['name', 'map'];
    protected $table = 'museumstraat';

}

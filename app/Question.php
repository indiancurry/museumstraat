<?php namespace Museumstraat;

use Illuminate\Database\Eloquent\Model;

class Question extends Model {

	protected $fillable = ['question', 'A', 'B', 'C', 'D', 'correctAnswer', 'value'];
    public $timestamps = true;

}

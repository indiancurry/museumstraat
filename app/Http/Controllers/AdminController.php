<?php namespace Museumstraat\Http\Controllers;

use Museumstraat\Http\Requests;
use Museumstraat\Http\Controllers\Controller;
use Museumstraat\User;
use Museumstraat\Museumstraat;
use Museumstraat\Question;

use Illuminate\Http\Request;

class AdminController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('admin.index');
	}

    public function museumstraat()
    {
        $museumstraat = Museumstraat::all();
        return view('admin.museumstraat')->with('museumstraat', $museumstraat);
    }

    public function storeMuseumstraat()
    {
        $museumstraat = new Museumstraat;
        $museumstraat->name = \Input::get('name');
        $museumstraat->save();
        return redirect('/admin/museumstraat');
    }

    public function questions()
    {
        $vragen = Question::all();

        return view('admin.questions')->with('vragen', $vragen);
    }

    public function postQuestion()
    {
        $museumstraat = new Question;
        $museumstraat->question = \Input::get('question');
        $museumstraat->A = \Input::get('A');
        $museumstraat->B = \Input::get('B');
        $museumstraat->C = \Input::get('C');
        $museumstraat->D = \Input::get('D');
        $museumstraat->correctAnswer = \Input::get('correctAnswer');
        $museumstraat->value = \Input::get('value');
        $museumstraat->save();
        return redirect('/admin/vragen');
    }

    public function users()
    {
        $users = User::all();

        return view('admin.users')->with('users', $users);
    }

    public function makeAdmin($id)
    {
        $user = User::find($id);
        $user->role = 1;
        $user->save();

        return redirect('/admin/gebruikers');
    }

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}

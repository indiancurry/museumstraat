<?php namespace Museumstraat\Http\Controllers;

use Museumstraat\Http\Requests;
use Museumstraat\Http\Controllers\Controller;
use Museumstraat\UserHasQuestion;
use Museumstraat\Question;
use Museumstraat\UserHasParked;

use Illuminate\Http\Request;

class QuizController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('quiz.index');
	}

    public function question()
    {
        $user = \Auth::user();

        $userQuestionsDone = UserHasQuestion::where('user_id', '=', $user->id)->get();

        $done = [];

        foreach($userQuestionsDone as $q)
        {
            $done[] = $q->question_id;
        }

        $totalQuestions = count(Question::all());


        $question = Question::whereNotIn('id', $done)->first();
        if(empty($question)){
            $q = UserHasParked::where('user_id', '=', $user->id)->first();
            if(!empty($q)){
                $question = Question::find($q->id);
            }else{
                return view('quiz.done');
            }
        }


        return view('quiz.question')->with(['question' => $question, 'total' => $totalQuestions]);
    }

    public function rightAnswer()
    {
        if ( \Session::token() !== \Input::get( '_token' ) ) {
            return \Response::json([
                "code" => 403,
                "msg" => 'Unauthorized attempt to update database.'
            ]);
        }

        $user = \Auth::user();

        $userHasQuestion = new UserHasQuestion;
        $userHasQuestion->user_id = $user->id;
        $userHasQuestion->question_id = \Input::get('questionID');
        $userHasQuestion->save();

        $user->score += \Input::get('value');
        $user->save();

        return \Response::json([
            "code" => 200,
            "msg" => 'User geupdate + score!',
        ]);
    }

    public function wrongAnswer()
    {
        if ( \Session::token() !== \Input::get( '_token' ) ) {
            return \Response::json([
                "code" => 403,
                "msg" => 'Unauthorized attempt to update database.'
            ]);
        }

        $user = \Auth::user();

        $userHasQuestion = new UserHasQuestion;
        $userHasQuestion->user_id = $user->id;
        $userHasQuestion->question_id = \Input::get('questionID');
        $userHasQuestion->save();

        return \Response::json([
            "code" => 200,
            "msg" => 'User geupdate!',
        ]);
    }

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}

<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'HomeController@index');
Route::get('/test', 'HomeController@test');

//Quiz routes
Route::group(['prefix' => 'quiz', 'middleware' => 'auth'], function(){
    Route::get('/', 'QuizController@question');
    Route::post('/rightAnswer', 'QuizController@rightAnswer');
    Route::post('/wrongAnswer', 'QuizController@wrongAnswer');
});

Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function(){
    Route::get('/', 'AdminController@index');
    Route::get('/museumstraat', 'AdminController@museumstraat');
    Route::post('/storeMuseumstraat', 'AdminController@storeMuseumstraat');
    Route::get('/vragen', 'AdminController@questions');
    Route::post('/postQuestion', 'AdminController@postQuestion');
    Route::get('/gebruikers', 'AdminController@users');
    Route::get('/makeAdmin/{id}', 'AdminController@makeAdmin');
});

Route::get('/makeAdmin/{id}', 'AdminController@makeAdmin');


//Map routes
Route::get('/plattegrond', 'MapController@index');

//Leaderboard routes
Route::get('/ranglijst', 'LeaderboardsController@index');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

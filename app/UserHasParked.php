<?php namespace Museumstraat;

use Illuminate\Database\Eloquent\Model;

class UserHasParked extends Model {

    protected $fillable = ['user_id', 'question_id'];
    protected $table = 'user_has_parked';
    public $timestamps = false;

}

<?php namespace Museumstraat;

use Illuminate\Database\Eloquent\Model;

class UserHasQuestion extends Model {

    protected $fillable = ['user_id', 'question_id'];
    protected $table = 'user_has_question';
    public $timestamps = false;

}

<?php


use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Museumstraat\User;

class UserTableSeeder extends Seeder {

    public function run()
    {
        $user = new User;
        $user->name = "Wouter van Unen";
        $user->email = "vanunen.wouter@gmail.com";
        $user->password = \Hash::make('changeme');
        $user->role = 1;
        $user->score = 0;
        $user->save();

        $user1 = new User;
        $user1->name = "Mark van der Weijden";
        $user1->email = "markvdweijden@gmail.com";
        $user1->password = \Hash::make('changeme');
        $user1->role = 1;
        $user1->score = 0;
        $user1->save();

        $user2 = new User;
        $user2->name = "Jimmy van den IJssel";
        $user2->email = "jimvdijssel@gmail.com";
        $user2->password = \Hash::make('changeme');
        $user2->role = 1;
        $user2->score = 0;
        $user2->save();

        $user3 = new User;
        $user3->name = "Thijs Rentier";
        $user3->email = "thijsrentier@gmail.com";
        $user3->password = \Hash::make('changeme');
        $user3->role = 1;
        $user3->score = 0;
        $user3->save();

        $user4 = new User;
        $user4->name = "Thomas Vreeken";
        $user4->email = "thomas.vreeken@gmail.com";
        $user4->password = \Hash::make('changeme');
        $user4->role = 1;
        $user4->score = 0;
        $user4->save();

    }

}